import React from 'react';
import { Box } from '@material-ui/core';
import UserList from '../../components/UserList/UserList'
import Navbar from '../../components/Navbar/Navbar';

export default function UserListPage(){
    return(
        <Box>
            <Navbar />
            <Box>
                <UserList />
            </Box>
        </Box>
    )
}