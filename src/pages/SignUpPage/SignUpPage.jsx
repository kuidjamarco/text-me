import React from 'react';
import { Box } from '@material-ui/core';
import SignUp from '../../components/SignUp/SignUp';
import Navbar from '../../components/Navbar/Navbar';
import { injectIntl } from 'react-intl';

export default injectIntl(function SignUpPage(props){

    let {formatMessage} = props.intl;
    return(
        <Box >
          <Navbar title={formatMessage({id:'signUpMessage'})}/>
          <Box className={props.class}>
            <SignUp/>
          </Box>
        </Box>
    )
})