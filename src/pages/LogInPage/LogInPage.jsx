import React from 'react';
import { Box } from '@material-ui/core';
import LogIn from '../../components/LogIn/LogIn';
import Navbar from '../../components/Navbar/Navbar'
import { injectIntl} from 'react-intl';


export default injectIntl(function SignInPage(props){

  let {formatMessage} = props.intl;
    return(
        <Box >
          <Navbar title={formatMessage({id:'logInMessage'})}/>
          <Box className={props.class}>
            <LogIn />
          </Box>
        </Box>
    )
})