import { ThemeProvider, makeStyles} from '@material-ui/core';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { IntlProvider } from 'react-intl';
import { useContext} from 'react'
import theme from '../ui/theme';
import SignUpPage from './SignUpPage/SignUpPage';
import SignInPage from './LogInPage/LogInPage';
import UserListPage from './UserListPage/UserListPage';
import UserContextProvider from '../contexts/userContext/User.provider';
import LangContext from '../contexts/langContext/Lang.context';
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

//css styling with makeStyle
const useStyles = makeStyles(theme=>({
    Body:{
      textAlign:'center',
      display:'flex',
      flexDirection:'colunm',
      justifyContent: 'center',
      color:'white',
      marginTop: '100px',
    }
}))

function App() {
  let classes = useStyles();

  let langContext = useContext(LangContext);
  let {lang} = langContext;

  return (
    <BrowserRouter>
      <ToastContainer />
      <IntlProvider locale={lang.lang} messages={lang.langMessage} >
        <ThemeProvider theme={theme}>
        <UserContextProvider>
          <Switch>
            <Route path='/' exact render={() => <SignUpPage class={classes.Body}/>}  />
            <Route path='/signUp'exact render={() => <SignUpPage class={classes.Body}/>}  />
            <Route path='/logIn' render={() => <SignInPage class={classes.Body}/>}  />
            <Route path='/home' render={() => <UserListPage class={classes.Body}/>}  />
          </Switch>
        </UserContextProvider>
        </ThemeProvider>
      </IntlProvider>
    </BrowserRouter>
  );
}

export default App;
