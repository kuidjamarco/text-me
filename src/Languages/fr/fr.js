const fr = {
    home: "Acceuil",
    signUpMessage:" Inscription",
    logInMessage:' Connexion',
    emailFieldMessage:'Entrer votre email',
    emailFieldErrorMessage:"email n'est pas valid",
    passwordFieldMessage:"Entrer votre mot de passe",
    passwordFieldErrorMessage:"le mot de passe doit contenir au moins 8 caractères",
    confirmFieldMessage:'confirm your password',
    confirmFieldErrorMessage:"le mot de passe doit correspondre à celui du champ *Password",
    nameLabelMessage:"Nom",
    passwordLabelMessage:"Mot de passe",
    confirmLabelMessage:"Confirmer votre mot de passe",
    emailLabelMessage:"Email",
    nameFieldMessage:'Entrer votre nom',
    nameFieldErrorMessage:"le nom doit contenir au moins 4 caractères",
    fieldRequiredMessage:"Ce champ est obligatoire",
    searchFieldMessage:"Trier par nom ou par email",
    signUpRegisterMessage:"se connecter à son compte",
    logInRegisterMessage:"ouvrir un compte",
    signUpBtnMessage:"S'inscrire",
    logInBtnMessage:"Se connecter",
    notifySignUpSuccess:"Votre compte a été créé avec succès",
    notifySignUpFailure: "Il y a eu un problème lors de la création de votre compte. Veuillez réessayer à nouveau"

}

export default fr;
