const en= {
    home: "Home",
    signUpMessage:" Sign up",
    logInMessage:'Sign In',
    emailFieldMessage:'Enter your email',
    emailFieldErrorMessage:"this email is not valid",
    passwordFieldMessage:"Enter your password",
    passwordFieldErrorMessage:"the password most contain at least 8 characters",
    confirmFieldMessage:'confirm your password',
    confirmFieldErrorMessage:"Passwords must match the one given in the field above",
    nameLabelMessage:"Name",
    passwordLabelMessage:"Password",
    confirmLabelMessage:"Confirm Password",
    emailLabelMessage:"Email",
    nameFieldMessage:'Enter your name',
    nameFieldErrorMessage:'The given name most be 4 or above characters',
    fieldRequiredMessage:'this field is obligatry',
    signUpRegisterMessage:'I already have an account',
    logInRegisterMessage:"I don't have an account",
    searchFieldMessage:"Search user by name or by email",
    signUpBtnMessage:"Sign Up",
    logInBtnMessage:"Sign In",
    notifySignUpSuccess:"Your account was created Successfully",
    notifySignUpFailure: "There was a problem creating your account. please try again"
}

export default en