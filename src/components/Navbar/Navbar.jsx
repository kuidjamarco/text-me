import logo from '../../logo.png';
import Profil from '../Profil/Profil.jsx';
import frMessage from '../../Languages/fr/fr';
import enMessage from '../../Languages/en/en';
import { makeStyles, Box, Select,  MenuItem, FormControl } from '@material-ui/core';
import { useContext } from 'react';
import LangContext from '../../contexts/langContext/Lang.context';

//M-ui css styling
const useStyles = makeStyles(theme => ({
    Navbar:{
        width:'100vw',
        height:'70px',
    },
    Logo:{
        position:'relative',
        height:'50px',
        left:'50px',
        top:'10px',
        [theme.breakpoints.down('sm')]:{
            left:'2px'
        }
    },
    Select:{
        position:'relative',
        left:'92%',
        top:'-180%',
        [theme.breakpoints.down('md')]:{
            position:'relative',
            left:'90%',
            top:'-170%'
        },
        [theme.breakpoints.down('sm')]:{
            left:'80%',
            top:'-120%'
        }
    },
}))

export default function Navbar(props) {
    let classes = useStyles();

    let langContext = useContext(LangContext);
    let {lang, setLang} = langContext;

    const handleChange = (event) => {
        let newLang = lang;
        if(event.target.value === 'fr')
            newLang = {lang:'fr', langMessage:frMessage};
        else newLang = {lang:'en', langMessage:enMessage};
        setLang(newLang)
    }

    return (
        <Box className={classes.Navbar} bgcolor='primary.main'>
            <img 
                src= {logo}
                alt="Text Me"
                className={classes.Logo}/>
            <Profil profil={logo} title={props.title}/>
            <FormControl className={classes.Select}>
                <Select
                    labelId='lang'
                    id={lang.lang}
                    value={lang.lang}
                    onChange={handleChange}
                >
                    <MenuItem value='fr'>Français</MenuItem>
                    <MenuItem value='en'>English</MenuItem>
                </Select>
            </FormControl>
        </Box>
    )
}