import React from 'react'
import {Button, TextField, Typography, Box} from '@material-ui/core';
import { useFormik } from 'formik';
import * as yup from 'yup';
import {Link} from 'react-router-dom';
import { injectIntl } from 'react-intl';
import useStyles from '../auth.useStyle.jsx';
import {notifySuccess, notifyError} from '../../utils/toastMessages';
import randomNumber from '../../utils/randomNumber.js';

function SignUp(props){

    let classes = useStyles();
    //translator
    let {formatMessage} = props.intl

    const validationSchema = yup.object({
        name: yup
            .string(formatMessage({id:'nameFieldMessage'}))
            .min(4, formatMessage({id:'nameFieldErrorMessage'}))
            .required(formatMessage({id:'fieldRequiredMessage'})),
        email: yup
            .string(formatMessage({id:'emailFieldMessage'}))
            .email(formatMessage({id:'emailFieldErrorMessage'}))
            .required(formatMessage({id:'fieldRequiredMessage'})),
        password: yup
            .string(formatMessage({id:'passwordFieldMessage'}))
            .min(8, formatMessage({id:'passwordFieldErrorMessage'}))
            .required(formatMessage({id:'fieldRequiredMessage'})),
        confirm: yup
            .string(formatMessage({id:'confirmFieldMessage'}))
            .required(formatMessage({id:'fieldRequiredMessage'}))
            .oneOf([yup.ref('password'), null], formatMessage({id:'confirmFieldErrorMessage'})),
    });
    
    const formik = useFormik({
        initialValues: {
            name: '',
            email: '',
            password: '',
            confirm: '',
        },
        validationSchema: validationSchema,
        onSubmit: (values, {resetForm}) => {
            formik.isSubmitting=true

            if(randomNumber(0,10)>=5){
                notifySuccess(formatMessage({ id:"notifySignUpSuccess" }))
                console.log(JSON.stringify(values, null, 2));
                resetForm()
            }else{
                notifyError(formatMessage({ id:"notifySignUpFailure" }))
            }
            formik.isSubmitting=false
            // fetch()
            // .then
            // .catch
            //we do threatment here if here is ok
        },
    })
    
    return(
            <form onSubmit={formik.handleSubmit}>
                <Box 
                    className={classes.Box}
                >
                <TextField
                        id='name'
                        name='name'
                        label={formatMessage({ id:"nameLabelMessage" })}
                        variant="outlined"
                        value={formik.values.name}
                        onChange={formik.handleChange}
                        className={classes.inputTextfield}
                        error={formik.touched.name && Boolean(formik.errors.name)}
                        helperText={formik.touched.name && formik.errors.name}
                    />
                </Box>
                <Box
                    className={classes.Box}
                >
                    <TextField
                        id='email'
                        name='email'
                        label={formatMessage({ id:"emailLabelMessage" })}
                        variant="outlined"
                        value={formik.values.email}
                        onChange={formik.handleChange}
                        className={classes.inputTextfield}
                        error={formik.touched.email && Boolean(formik.errors.email)}
                        helperText={formik.touched.email && formik.errors.email}
                    />
                </Box>
                <Box
                    className={classes.Box}
                >
                    <TextField
                        id='password'
                        name='password'
                        label={formatMessage({ id:"passwordLabelMessage" })}
                        type='password'
                        variant="outlined"
                        value={formik.values.password}
                        onChange={formik.handleChange}
                        className={classes.inputTextfield}
                        error={formik.touched.password && Boolean(formik.errors.password)}
                        helperText={formik.touched.password && formik.errors.password}
                    />
                </Box>
                <Box
                    className={classes.Box}
                >
                    <TextField
                        id='confirm'
                        name='confirm'
                        label={formatMessage({ id:"confirmLabelMessage" })}
                        type='password'
                        variant="outlined"
                        value={formik.values.confirm}
                        onChange={formik.handleChange}
                        className={classes.inputTextfield}
                        error={formik.touched.confirm && Boolean(formik.errors.confirm)}
                        helperText={formik.touched.confirm && formik.errors.confirm}
                    />
                </Box>
                <Typography className={classes.caption}>
                    <Link color="primary" to="/logIn"> {formatMessage({id:'signUpRegisterMessage'})}</Link>
                </Typography>
                <Button className={classes.Button} variant="contained" disabled={formik.isSubmitting} color="primary" type="submit"> 
                    {formatMessage({id:'signUpBtnMessage'})}
                </Button>
            </form>
    );
}

export default injectIntl(SignUp);