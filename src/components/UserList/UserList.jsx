import {Box, Typography} from '@material-ui/core';
import Profil from '../Profil/Profil';

function UserList(props){
    return(
        <Box>
            <Profil />
            <Box>
                <Typography variant='p'>
                    <b>{props.nbrOfMessage}</b><br/>{props.Status}
                </Typography>
            </Box>
        </Box>
    )
}

export  default UserList;