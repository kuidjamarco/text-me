import {useContext} from 'react';
import { injectIntl } from 'react-intl';
import Layout from '../../hoc/Layout/Layout';
import { TextField, makeStyles, Typography, Box} from '@material-ui/core';
import { useFormik } from 'formik';
import UserContext from '../../contexts/userContext/User.context';


const useStyles = makeStyles((theme) => ({
    inputTextfield:{
         borderRadius:'20px',
         width:'90%',
         height:'40px',
    },
    SearchBar:{
        position:'relative',
        left:'3%',
        top:'-40px',
        width:'50%'
    },
    Logo:{
        height:'40px',
        position:'relative',
        top:'-65px',
        left:'62%',
        [theme.breakpoints.down('md')]:{
            left:'58%'
        },
        [theme.breakpoints.down('sm')]:{
            left:'50%',
        }
    },
    Status:{
        fontSize:'15px',
    },
    AuthTitle:{
        position:'relative',
        top:'-70px',
        width:'50%',
        textAlign:'center',
        fontSize:theme.typography.fontWeightLight/6,
        [theme.breakpoints.down('sm')]:{
            fontSize:theme.typography.fontWeightLight/10,
            top:'-60px',
            left:'-50px',
        },
        [theme.breakpoints.down('xs')]:{
            fontSize:theme.typography.fontWeightLight/20,
            left:'-40px'
        }
    },
    Body:{
        fontSize: 'calc(9px + 2vmin)',
        color:'white',
        display:'flex',
        flexDirection:'colunm',
        alignItems:'center',
        justifyContent:'center',
        [theme.breakpoints.down('sm')]:{
            position:'relative',
            top:'20px',
            left:'-35px'
        }
    },
    Profil:{
        position:'relative',
        left:'12.5%',
        top: '-65px',
        [theme.breakpoints.down('md')]:{
            left:'8%',
        },
        [theme.breakpoints.down('sm')]:{
            left:'0%',
        },
    }
}))

function Profil(props){
    let classes = useStyles();

    let userContext = useContext(UserContext);
    let {user} = userContext
    let {formatMessage} = props.intl;

    const formik = useFormik({
        initialValues:{
            search:formatMessage({id:'searchFieldMessage'})
        },
        onSubmit:(values) => {
            //do to code here
        },
    })

    let searchBar =
        <Typography variant='h6' className={classes.AuthTitle}>
            { props.title }
        </Typography>;
    if(user.isConnected){
        searchBar =  (
            <Box className={classes.SearchBar} >
                <form onSubmit={formik.handleSubmit}>
                    <TextField
                        id='search'
                        name='search'
                        value={formik.values.search}
                        onChange={formik.handleChange}
                        className={classes.inputTextfield}
                    />
                </form>
            </Box>
        )
    }
    

    return(
        <Layout className={classes.Body}>
            <img
                src={props.profil}
                alt='Mon profil'
                className={classes.Logo}/>
            {searchBar}
            <Box className={classes.Profil}>
                <Typography>{user.name}</Typography>
                <Typography className={classes.Status}>
                    {user.status}
                </Typography>
            </Box>
        </Layout>
    );
}

export default injectIntl(Profil);