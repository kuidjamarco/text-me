import { makeStyles } from "@material-ui/core";


export default makeStyles(theme=>({
    inputTextfield:{
        backgroundColor:theme.palette.secondary.main,
        width:'55vw',
        border:'none',
        [theme.breakpoints.down('sm')]:{
            width:'70vw'
        }
    },
    Box: {
        marginBottom:'20px'
    },
    Button:{
        position:'relative',
        width:'30%',
        marginTop:"10px",
        [theme.breakpoints.down('sm')]:{
            width:'70%'
        }
    },
    caption:{
        textAlign:'left',
        position:'relative',
        top:'-6px'
    }
}));