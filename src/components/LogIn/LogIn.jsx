import {Button, TextField, Typography, Box, CircularProgress} from '@material-ui/core';
import { useFormik } from 'formik';
import {useState} from 'react';
import * as yup from 'yup';
import {Link} from 'react-router-dom';
import { injectIntl } from 'react-intl';
import useStyles from '../auth.useStyle.jsx';
import randomNumber from '../../utils/randomNumber.js';
import { notifySuccess, notifyError } from '../../utils/toastMessages.jsx';

function LogIn(props) {

    let classes = useStyles();
    let [isSubmitting, setIsSubmitting]= useState(false)
    
    //translator
    let {formatMessage} = props.intl

    const validationSchema = yup.object({
        email: yup
            .string(formatMessage({id:'emailFieldMessage'}))
            .email(formatMessage({id:'emailFieldErrorMessage'})),
        password: yup
            .string(formatMessage({id:'passwordFieldMessage'}))
    })

    const formik = useFormik({
        initialValues:{
            email:'',
            password: ''
        },
        validationSchema:validationSchema,
        onSubmit: (values, {resetForm}) => {

            setIsSubmitting(true)
            setTimeout(() => {
                if(randomNumber(0, 10) >= 5){
                    notifySuccess(formatMessage({id:'notifySignUpSuccess'}))
                    console.log(JSON.stringify(values, null, 2));
                    resetForm()
                }else{
                    notifyError(formatMessage({ id:"notifySignUpFailure" }))
                }
                setIsSubmitting(false)
            }, 3000);
            //alert(JSON.stringify(values, null, 2));
        }

    })
    return(
        <form onSubmit={formik.handleSubmit}>
            <Box 
                className={classes.Box}
            >
                <TextField
                    id='email'
                    name='email'
                    label={formatMessage({ id:"emailLabelMessage" })}
                    variant="outlined"
                    value={formik.values.email}
                    onChange={formik.handleChange}
                    className={classes.inputTextfield}
                    error={formik.touched.email && Boolean(formik.errors.email)}
                    helperText={formik.touched.email && formik.errors.email}
                 />
            </Box>
            <Box>
                <TextField
                    id='password'
                    name='password'
                    label={formatMessage({ id:"passwordLabelMessage" })}
                    variant="outlined"
                    value={formik.values.password}
                    onChange={formik.handleChange}
                    className={classes.inputTextfield}
                    error={formik.touched.password && Boolean(formik.errors.password)}
                    helperText={formik.touched.password && formik.errors.password}
                />
            </Box>
            {isSubmitting
                ?null
                :<Typography className={classes.caption}>
                    <Link color="primary" to="/SignUp">{formatMessage({id:'logInRegisterMessage'})}</Link>
                </Typography>
            }
            <Button className={classes.Button} variant="contained" disabled={isSubmitting} color="primary" type="submit">
                {isSubmitting && <CircularProgress style={{marginRight:"10px"}} size={25} />}
                {formatMessage({id:'logInBtnMessage'})}
            </Button>
        </form>
    )
}

export default injectIntl(LogIn);