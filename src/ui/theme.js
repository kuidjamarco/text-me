import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette:{
        primary: {
            main: "#1A8725"
        },
        secondary: {
            main: "#E0D6D2"
        }
    },
    typography:{
        fontFamily:"'Raleway', 'Cambria', 'Roboto', 'Helvetica', 'Arial', 'sans-serif'"
    }
})

export default theme;