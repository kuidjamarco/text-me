import { useState } from "react";
import LangContext from "./Lang.context";
import enMessage from '../../Languages/en/en';

function LangContextProvider({children}){
    let [lang, setLang] = useState({
        lang:'en',
        langMessage:enMessage
    });
    let value = {lang, setLang}
    return (
        <LangContext.Provider value={value} >
            {children}
        </LangContext.Provider>
    )
}

export default LangContextProvider;