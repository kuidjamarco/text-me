import { useState } from "react";
import UserContext from "./User.context";

function UserContextProvider({children}){
    let [user, setUser] = useState({
        name:"Text Me",
        isRegistered:false,
        isConnected:false,
        status:"Online"
    })

    let value = {user, setUser}
    return (
        <UserContext.Provider value={value} >
            {children}
        </UserContext.Provider>
    )
}

export default UserContextProvider;