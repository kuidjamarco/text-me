const Layout = (props) => <div className={props.className}>{props.children}</div>

export default Layout;